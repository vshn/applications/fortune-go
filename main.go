package main

import (
	"bytes"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"os"
	"os/exec"
)

var (
	fortuneGenerated = promauto.NewCounter(prometheus.CounterOpts{
		Name: "fortune_generated_message",
		Help: "The total number of generated messages",
	})
)

// tag::router[]
func setupRouter() *gin.Engine {
	r := gin.Default()
	gin.DisableConsoleColor()
	r.LoadHTMLFiles("templates/fortune.html")
	hostname := hostname()
	version := "1.4-go"

	r.GET("/sleep", func(c *gin.Context) {
		contentType, response := buildResponse(c, hostname, version, true)
		respond(c, contentType, response)
	})

	r.GET("/", func(c *gin.Context) {
		contentType, response := buildResponse(c, hostname, version, false)
		respond(c, contentType, response)
	})

	r.GET("/metrics", gin.WrapH(promhttp.Handler()))

	return r
}

func respond(c *gin.Context, contentType string, response gin.H) {
	if contentType == "application/json" {
		c.JSON(http.StatusOK, response)
		return
	}
	if contentType == "text/plain" {
		c.String(http.StatusOK, "Fortune %s cookie of the day #%d:\n\n%s", response["version"], response["number"], response["message"])
		return
	}
	c.HTML(http.StatusOK, "fortune.html", response)
}

// end::router[]

func main() {
	r := setupRouter()
	r.Run(":8080")
}

func buildResponse(c *gin.Context, hostname string, version string, sleep bool) (string, gin.H) {
	start := time.Now()
	contentType := c.Request.Header.Get("Accept")
	if sleep {
		time.Sleep(time.Duration(random(6)) * time.Second)
	}
	fortune := fortune()
	random := random(1000)
	elapsed := time.Since(start).Seconds()
	response := gin.H{
		"message":  fortune,
		"hostname": hostname,
		"number":   random,
		"version":  version,
		"seconds":  elapsed,
	}
	return contentType, response
}

func fortune() string {
	fortuneGenerated.Inc()
	cmd := exec.Command("fortune")

	cmd.Stdin = strings.NewReader("")

	var out bytes.Buffer
	cmd.Stdout = &out

	err := cmd.Run()

	if err != nil {
		log.Fatal(err)
	}

	return out.String()
}

func random(max int64) int64 {
	rand.Seed(time.Now().UnixNano())
	return rand.Int63n(max)
}

func hostname() string {
	hostname, err := os.Hostname()
	if err != nil {
		return ""
	}
	return hostname
}
