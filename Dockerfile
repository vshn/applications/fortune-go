# Step 1: build binary
FROM alpine:3.14 AS build
RUN apk update && apk upgrade && apk add --no-cache go gcc g++
WORKDIR /app
# ENV GOPATH /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY *.go ./
RUN CGO_ENABLED=1 GOOS=linux go build

# tag::production[]
# Step 2: deployment image
FROM alpine:3.14
RUN apk update && apk upgrade && apk add --no-cache fortune
WORKDIR /app
COPY --from=build /app/fortune-go /app/fortune-go
COPY templates /app/templates

EXPOSE 8080

# <1>
USER 1001

ENTRYPOINT ["/app/fortune-go"]
# end::production[]
